from PyQt5.QtWidgets import QMainWindow, QApplication,QDialog
from PyQt5 import uic
from PyQt5.QtCore import Qt


class Puntaje(QMainWindow):
    def __init__(self,parent):
        super().__init__(parent=parent)
        uic.loadUi("ui/highScores.ui", self)

        self.botonAtras.clicked.connect(self.cerrarymostrar)
    
    def cerrarymostrar(self):
        self.parent().prueba="Hola"
        self.close()
        self.parent().show()
    def closeEvent(self, event):
        self.close()
        self.parent().show()