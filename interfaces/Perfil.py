from PyQt5.QtWidgets import QMainWindow, QApplication,QDialog,QInputDialog
from PyQt5 import uic
from PyQt5.QtCore import Qt
import sqlite3

class Perfil(QDialog):
    def __init__(self,parent):
        super().__init__(parent=parent)
        uic.loadUi("ui/perfil.ui", self)
        self.conexion=sqlite3.connect('src/players.db')
        self.cursor=self.conexion.cursor()

        self.botonNuevo.clicked.connect(self.nuevoPerfil)
        self.botonElegir.clicked.connect(self.elegirPerfil)

    def nuevoPerfil(self):
        jugador,Ok=QInputDialog.getText(self,'Ingresar','Ingrese tu nombre o nickname')
        if len(jugador)!=0:
            self.cursor.execute(""" INSERT INTO jugador
                                (nombre,puntos)
                                VALUES (?,?)
                                 """,(jugador,0))
            self.conexion.commit()
            self.lista.addItem(jugador)
    def elegirPerfil(self):
        if self.lista.currentItem()==None:
            jugador="Player"
            self.parent().labelPerfil.setText(jugador)
            self.close()
        else:
            jugador=str(self.lista.currentItem().text())
            self.parent().labelPerfil.setText(jugador)
            self.close()
        
        if self.radiobtnDesafio.isChecked():
            self.parent().modoJuego=0
            print("DESAFIO!!!")
        elif self.radiobtnSumas.isChecked():
            self.parent().modoJuego=1
            print("SUMAS")
        elif self.radiobtnRestas.isChecked():
            self.parent().modoJuego=2
        elif self.radiobtnProductos.isChecked():
            self.parent().modoJuego=3
        elif self.radiobtnCocientes.isChecked():
            self.parent().modoJuego=4
        
        self.parent().repaint()

    def closeEvent(self, event):
        if self.lista.currentItem()==None:
            self.parent().labelPerfil.setText("Player")
        self.parent().juego()
        self.parent().repaint()
        
    def showEvent(self,event):        
        self.lista.clear()       
        self.cursor.execute("SELECT * FROM jugador")
        gamers=self.cursor.fetchall()
        for gamer in gamers:
            self.lista.addItem(gamer[1])



