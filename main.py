from PyQt5.QtWidgets import QMainWindow,QApplication,QInputDialog,QMessageBox,QDialog
from PyQt5 import uic
from PyQt5 import QtMultimedia
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sqlite3
from interfaces.HowToPlay import Reglas
from interfaces.HighScore import Puntaje
from interfaces.Game import Juego
from interfaces.Perfil import Perfil

class Main(QMainWindow):
    def __init__(self,):
        super().__init__()
        uic.loadUi("ui/main.ui",self)

        self.sin_musica=True
        self.dirMp3=QUrl.fromLocalFile('./src/music.m4a')
        self.contenido=QtMultimedia.QMediaContent(self.dirMp3)
        self.reproductor=QtMultimedia.QMediaPlayer()
        self.reproductor.setMedia(self.contenido)

        self.game=Juego(self)
        self.reglas=Reglas()
        self.scores=Puntaje(self)

        self.perfilelegido=""
        self.elegirPerfil=Perfil(self)       

        self.prueba=""        

        self.botonSobreNosotros.clicked.connect(self.mod_python_clicked)
        self.botonSobreNosotros.pressed.connect(self.mod_python_hold)
        self.botonSobreNosotros.released.connect(self.mod_python_loose)        
        self.botonMusica.clicked.connect(self.musica_control)

        self.botonJugar.clicked.connect(self.jugar)
        self.botonJugar.pressed.connect(self.jugar_hold)
        self.botonJugar.released.connect(self.jugar_loose)

        self.botonSalir.clicked.connect(self.salir)
        self.botonSalir.pressed.connect(self.salir_hold)
        self.botonSalir.released.connect(self.salir_loose)
        self.botonScores.clicked.connect(self.high_scores)
        self.botonScores.pressed.connect(self.high_scores_hold)
        self.botonScores.released.connect(self.high_scores_loose)
        self.botonComoJugar.clicked.connect(self.como_jugar)


    def mod_python_clicked(self):
        msg=QMessageBox()        
        msg.setWindowTitle('Sobre nosotros...')
        msg.setWindowIcon(QIcon('./img/pi.png'))
        msg.setText("""
            MathPlay v0.1           
            -------------               

            Consultora: A colocar       
            Integrantes:                
            -> Santiago Avila       
            -> Juan Gramajo         
            -> Daniel Barros        
                    """)
        msg.setIcon(QMessageBox.Information)
        msg.exec_()
        print("Ok!")
    def mod_python_hold(self):
        self.labelPython.setGeometry(660,460,61,111)
        self.labelPython.setScaledContents(True)
    def mod_python_loose(self):
        self.labelPython.setGeometry(680,480,51,91)
        self.labelPython.setScaledContents(True)

    def musica_control(self):
        if self.sin_musica:
            self.labelMusica.setPixmap(QPixmap('./img/btn_stop.png')) 
            self.sin_musica=False          
            self.reproductor.play()            
            self.botonMusica.setToolTip("Desactivar Musica")
            print("Music On")
        else:
            self.labelMusica.setPixmap(QPixmap('./img/btn_play.png'))
            self.sin_musica=True
            self.reproductor.stop()
            self.botonMusica.setToolTip("Activar Musica")
            print("Music Off")

    def jugar(self):
        print("Jugar!!!")
        print(str(self.prueba))
        self.hide()        
        self.game.show()
    def jugar_hold(self):
        self.labelJugar.setGeometry(270,110,250,90)
        self.labelJugar.setScaledContents(True)
    def jugar_loose(self):
        self.labelJugar.setGeometry(280,120,230,70)
        self.labelJugar.setScaledContents(True)
      
    def salir_hold(self):
        self.labelSalir.setGeometry(270,400,250,90)
        self.labelSalir.setScaledContents(True)
    def salir_loose(self):
        self.labelSalir.setGeometry(280,410,230,70)
        self.labelSalir.setScaledContents(True)
    def salir(self):
        print("Salir :'(")
        quit()

    def high_scores(self):
        print("High Scores...")
        self.hide()
        self.scores.show()       
    def high_scores_hold(self):
        self.labelScores.setGeometry(180,260,450,90)
        self.labelScores.setScaledContents(True)
    def high_scores_loose(self):
        self.labelScores.setGeometry(190,270,430,70)
        self.labelScores.setScaledContents(True) 

    def como_jugar(self):
        self.reglas.show()
      
    def showEvent(self,event):
        conexion=sqlite3.connect('src/players.db')        
        cursor=conexion.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS jugador('+
        "ID INTEGER PRIMARY KEY autoincrement, "+
        "nombre VARCHAR(50), "+
        "puntos INTEGER"+
        ')')
        

app=QApplication([])

win=Main()
win.show()

app.exec_()